#include "bsdf.h"
#include "integrator.h"
#include "scene.h"
#include "frame.h"
#include "lights/areaLight.h"



class Direct_lms : public Integrator {
public:
  Direct_lms(const PropertyList &props) {
    m_sampleCount = props.getInteger("samples",1);
  }

  Color3f Li(const Scene *scene, Sampler *sampler, const Ray &ray) const {
    /* Find the surface that is visible in the requested direction */
    Hit hit;
    scene->intersect(ray, hit);
    if (!hit.foundIntersection())
      return scene->backgroundColor();

    Color3f radiance = Color3f::Zero();
    const BSDF *bsdf = hit.shape->bsdf();

    Normal3f normal = hit.localFrame.n;
    Point3f pos = ray.at(hit.t);

    // if(hit.shape->intersect(ray,hit)){
    //     radiance = AreaLight::intensity(hit,ray.direction);
    // }
    Color3f intensity = Color3f::Zero();
    const LightList &lights = scene->lightList();
    for (LightList::const_iterator it = lights.begin(); it != lights.end();++it) {
        for(int u=0; u<m_sampleCount; ++u) {
            for(int v=0; v<m_sampleCount; ++v){       
                //light->sample(ray.direction,hit.uv,)
                float dist, pdf;
                Vector3f lightDir;
                intensity = (*it)->intensity(hit,ray.direction);
                //intensity = (*it)->sample(pos, sampler->next2D(), pdf, lightDir, dist) ;
                Ray shadowRay(pos + normal * Epsilon, lightDir, true);
                Hit shadowHit;
                scene->intersect(shadowRay, shadowHit);
                if (!shadowHit.foundIntersection() || shadowHit.t > dist) {
                    float cos_term = std::max(0.f, lightDir.dot(normal));
                    Color3f brdf =
                        bsdf->eval(BSDFQueryRecord(hit.toLocal(-ray.direction),
                                                hit.toLocal(lightDir),
                                                ESolidAngle, hit.uv));
                    radiance += intensity * cos_term * brdf;

            }
        }

        radiance +=(*it)->intensity(hit,ray.direction);
      }
    }

    return radiance;
  }


private:
    int m_sampleCount;
  std::string toString() const { return "Direct_lms[]"; }
};

REGISTER_CLASS(Direct_lms, "direct_lms");
