#include "bsdf.h"
#include "core/warp.h"
#include "integrator.h"
#include "scene.h"


class AO : public Integrator {
public:
  AO(const PropertyList &props) {
    m_sampleCount = props.getInteger("sampleCount", 32);
    m_cosineWeighted = false;//props.getBoolean("cosineWeighted", true);
  }

  Color3f Li(const Scene *scene, Sampler *sampler, const Ray &ray) const {
    Hit hit;
    //lancé de rayon
    scene->intersect(ray, hit);
    if (!hit.foundIntersection())
      return scene->backgroundColor();
    
    const BSDF *bsdf = hit.shape->bsdf();

    Normal3f normal = hit.localFrame.n;
    Point3f pos = ray.at(hit.t);
    float sum = 0;
    for(int i = 0; i < m_sampleCount;i++) {
      Point2f dir = sampler->next2D();
      Vector3f vdir = Vector3f(0,0,0);
      if(m_cosineWeighted) {
        vdir = Warp::squareToCosineHemisphere(dir);
      }
      else {
        vdir = Warp::squareToUniformHemisphere(dir);
      }
      Vector3f vdirWorld = hit.toWorld(vdir);
      Normal3f n = hit.localFrame.n;
      Hit newHit;
      Ray r(pos + n * Epsilon,vdirWorld);
      scene->intersect(r, newHit);
      if (!newHit.foundIntersection()) {
        if(m_cosineWeighted) {
         sum += n.dot(vdirWorld / Warp::squareToCosineHemispherePdf(vdir));
        } else {
          sum += n.dot(vdirWorld / Warp::squareToUniformHemispherePdf(vdir));
        }
      }
    }
    return sum/(M_PI *m_sampleCount);
  }

  std::string toString() const {
    return tfm::format("AO[\n"
                       "  samples = %f\n"
                       "  cosine-weighted = %s\n"
                       " ]",
                       m_sampleCount, m_cosineWeighted );
  }

private:
  int m_sampleCount;
  bool m_cosineWeighted;
};

REGISTER_CLASS(AO, "ao")
