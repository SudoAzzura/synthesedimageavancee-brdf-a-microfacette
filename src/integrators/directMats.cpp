#include "bsdf.h"
#include "integrator.h"
#include "scene.h"
#include "frame.h"
#include "lights/areaLight.h"
#include "warp.h"



class DirectMats : public Integrator {
public:
  DirectMats(const PropertyList &props) {
    m_sampleCount = props.getInteger("sampleCount",100);
    m_is = props.getBoolean("IS",false);
    
  }

  Color3f Li(const Scene *scene, Sampler *sampler, const Ray &ray) const {
    /* Find the surface that is visible in the requested direction */
    Hit hit;
    scene->intersect(ray, hit);
    if (!hit.foundIntersection())
      return scene->backgroundColor(ray.direction);

    Color3f radiance = Color3f::Zero();
    const BSDF *bsdf = hit.shape->bsdf();

    Normal3f normal = hit.localFrame.n;
    Point3f pos = ray.at(hit.t);


    Color3f intensity = Color3f::Zero();
   
    for(int v=0; v<m_sampleCount; ++v){       
        //light->sample(ray.direction,hit.uv,)
        Point2f ps =sampler->next2D();
        Vector3f lightDir = Warp::squareToCosineHemisphere(ps);
        Vector3f light_world = hit.toWorld(lightDir);
        float pdf = Warp::squareToCosineHemispherePdf(lightDir);
        
        intensity = scene->backgroundColor(light_world);
        Ray shadowRay(pos + normal * Epsilon, light_world, false);
        Hit shadowHit;
        scene->intersect(shadowRay, shadowHit);
        if (!shadowHit.foundIntersection() ) {
            float cos_term = std::max(0.f, light_world.dot(normal));
            Color3f brdf =
                bsdf->eval(BSDFQueryRecord(hit.toLocal(-ray.direction),lightDir,ESolidAngle, hit.uv));
            if(m_is) {

              BSDFQueryRecord b = BSDFQueryRecord(hit.toLocal(-ray.direction),lightDir,ESolidAngle, hit.uv);
        
              pdf /= bsdf->pdf(b);
              brdf =  bsdf->sample(b,ps);

              if(pdf < 0.01 ) {
              radiance += brdf* cos_term *intensity ;
            }else {
              radiance += intensity * cos_term* (brdf/pdf);
            }    
            } else {
              if(pdf < 0.01 ) {
              radiance += intensity * cos_term * brdf ;
            }else {
              radiance += intensity * cos_term * brdf/pdf;
            }    
            }
                     
            

      }
    }
    // if(m_is) 
    //   return ((radiance).x()+(radiance).y() + (radiance).z())/radiance/m_sampleCount;
    return radiance/m_sampleCount;
  }


private:
    int m_sampleCount;
    bool m_is;
  std::string toString() const { return "direct_mats[]"; }
};

REGISTER_CLASS(DirectMats, "direct_mats");
