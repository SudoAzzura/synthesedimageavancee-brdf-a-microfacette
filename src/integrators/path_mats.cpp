#include "bsdf.h"
#include "integrator.h"
#include "lights/areaLight.h"
#include "scene.h"
#include "warp.h"

#define STRATIFIED

class PathMats : public Integrator {
public:
  PathMats(const PropertyList &props) {
  }

  Color3f Li(const Scene *scene, Sampler *sampler, const Ray &ray) const{
    Color3f radiance = Color3f::Zero();

    // Iterate over the path
    Ray r = ray;
    // int maxnb = 0;
    // int nb = 0;
    float continuationProb;
    float eta = 1.f;
    Hit hit;

      scene->intersect(ray, hit);

    if (!hit.foundIntersection()) {
        // If the ray doesn't intersect any surfaces, return the background color
      return scene->backgroundColor(ray.direction);
    }
    for(int depth = 0; depth<=256;depth++) {
      
      // Find the surface that is visible in the requested direction
      

      if(hit.shape->isAreaLight()){
          return radiance + hit.shape->areaLight()->intensity(hit,r.direction);
        }
      
    const BSDF *bsdf = hit.shape->bsdf();
    Normal3f normal = hit.localFrame.n;
    Point3f pos = ray.at(hit.t);
    
    BSDFQueryRecord query(hit.toLocal(-ray.direction));
    query.uv = hit.uv;
    Color3f weighted_brdf = bsdf->sample(query, sampler->next2D());
    Vector3f sampleDir = hit.toWorld(query.wo);
    if (sampleDir.dot(normal) < 0) { // transmission
      r = Ray(pos - normal * Epsilon, sampleDir);
    } else { // reflection
      r = Ray(pos + normal * Epsilon, sampleDir);
    }
    eta *= query.eta;
    r.recursionLevel = ray.recursionLevel + 1;
    float continuationProb = std::min(weighted_brdf.maxCoeff() * eta * eta, 0.99f);
    
    //radiance += weighted_brdf * Li(scene, sampler, r);
    if (depth >= 3 && sampler->next1D() > continuationProb) {
      // If the path is terminated, return the accumulated radiance
      //cout<<"rfin "<<radiance.x()<<" "<<radiance.y()<<" "<<radiance.z()<<"\n"<<endl;
      //return radiance/depth;
      return weighted_brdf * Li(scene, sampler, r);
    }
    weighted_brdf /= continuationProb;
      
      // Compute the probability of continuing the path using Russian roulette


      // Update the ray and the path radiance
      r = Ray(ray.at(hit.t) + hit.localFrame.n * Epsilon, r.direction, true);
      r.recursionLevel = depth + 1;
      
    }

    // If the maximum recursion depth is reached, return the accumulated radiance
    return radiance;
  }
  private :
  std::string toString() const { return "pathMats";}
};

REGISTER_CLASS(PathMats, "path_mats")