#include "bsdf.h"
#include "integrator.h"
#include "lights/areaLight.h"
#include "scene.h"
#include "warp.h"

#define STRATIFIED

class PathEms : public Integrator {
public:
  PathEms(const PropertyList &props) {
  }

  Color3f Li(const Scene *scene, Sampler *sampler, const Ray &ray) const{
    Color3f radiance = Color3f::Zero();
    Color3f intensity = Color3f::Zero();
    // Iterate over the path
    Ray r = ray;

    float continuationProb;
    float eta = 1.f;
    Hit hit;

      scene->intersect(ray, hit);

    if (!hit.foundIntersection()) {
      return scene->backgroundColor(ray.direction);
    }
    for(int depth = 0; depth<=512;depth++) {
      const LightList &lights = scene->lightList();
    for (LightList::const_iterator it = lights.begin(); it != lights.end();++it) {
      

      
      
    const BSDF *bsdf = hit.shape->bsdf();
    Normal3f normal = hit.localFrame.n;
    Point3f pos = ray.at(hit.t);
    
    BSDFQueryRecord query(hit.toLocal(-ray.direction));
    query.uv = hit.uv;
    Color3f weighted_brdf = bsdf->sample(query, sampler->next2D());
    Vector3f sampleDir = hit.toWorld(query.wo);
    if (sampleDir.dot(normal) < 0) { // transmission
      r = Ray(pos - normal * Epsilon, sampleDir);
    } else { // reflection
      r = Ray(pos + normal * Epsilon, sampleDir);
    }
    eta *= query.eta;
    r.recursionLevel = ray.recursionLevel + 1;
    
    
    //radiance += weighted_brdf * Li(scene, sampler, r);
    if (depth >= 3) {
      float continuationProb = std::min(weighted_brdf.maxCoeff() * eta * eta, 0.99f);
      if(sampler->next1D() > continuationProb){
        return weighted_brdf* Li(scene, sampler, r);
      }
      // If the path is terminated, return the accumulated radiance
      weighted_brdf /= continuationProb;
    }
    
      //light->sample(ray.direction,hit.uv,)
                float dist, pdf;
                Vector3f lightDir;
                intensity = (*it)->intensity(hit,ray.direction);
                //intensity = (*it)->sample(pos, sampler->next2D(), pdf, lightDir, dist) ;
                Ray shadowRay(pos + normal * Epsilon, lightDir, true);
                Hit shadowHit;
                scene->intersect(shadowRay, shadowHit);
                if (!shadowHit.foundIntersection() || shadowHit.t > dist) {
                    float cos_term = std::max(0.f, lightDir.dot(normal));
                    Color3f brdf =
                        bsdf->eval(BSDFQueryRecord(hit.toLocal(-ray.direction),
                                                hit.toLocal(lightDir),
                                                ESolidAngle, hit.uv));
                    radiance += intensity * cos_term * brdf;

            }
      // Compute the probability of continuing the path using Russian roulette

      // Update the ray and the path radiance
      //r = Ray(ray.at(hit.t) + hit.localFrame.n * Epsilon, r.direction, true);
      //r.recursionLevel = depth + 1;
    }
    }

    // If the maximum recursion depth is reached, return the accumulated radiance
    return radiance;
  }
  private :
  std::string toString() const { return "pathEms";}
};

REGISTER_CLASS(PathEms, "path_ems")
