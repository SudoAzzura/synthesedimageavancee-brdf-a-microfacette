
/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob
*/

#include "bsdf.h"
#include "core/warp.h"
#include "frame.h"
#include <math.h>


class Microfacet : public BSDF {
public:
  Microfacet(const PropertyList &propList) {
    /* RMS surface roughness */
    m_alpha = propList.getFloat("alpha", 0.1f);

    /* Interior IOR (default: BK7 borosilicate optical glass) */
    m_intIOR = propList.getFloat("intIOR", 1.5046f);

    /* Exterior IOR (default: air) */
    m_extIOR = propList.getFloat("extIOR", 1.000277f);

    /* Albedo of the diffuse base material (a.k.a "kd") */
    m_kd = propList.getColor("kd", Color3f(0.5f));

    /* To ensure energy conservation, we must scale the
       specular component by 1-kd.

       While that is not a particularly realistic model of what
       happens in reality, this will greatly simplify the
       implementation. */
    m_ks = 1 - m_kd.maxCoeff();
  }

  /// Evaluate the BRDF for the given pair of directions
  Color3f eval(const BSDFQueryRecord &bRec) const {
     if (bRec.measure != ESolidAngle || Frame::cosTheta(bRec.wi) <= 0 ||Frame::cosTheta(bRec.wo) <= 0)
        return Color3f(0.0f);
    Vector3f WI = bRec.wi;
    Vector3f WO = bRec.wo;

    Vector3f wh = (WI + WO).normalized();
    Vector3f n = Vector3f(0,0,1);

    float cosThetaI = Frame::cosTheta(WI);//WI.dot(n);//Frame::cosTheta(WI)
    float cosThetaO = Frame::cosTheta(WO);//WO.dot(n);//Frame::cosTheta(WO)
    float cosThetaH = Frame::cosTheta(wh);//wh.dot(n);//Frame::cosTheta(wh)
    float cosThetaT =0;// wh.dot(WO)
    float cosThetaD = wh.dot(WI);
    float F = fresnel(cosThetaD,m_extIOR,m_intIOR,cosThetaT);

    float DFG = D(wh) *F * G(WI,WO,wh,n);
    Color3f fr = (m_kd / M_PI) +  m_ks *(DFG/(4*cosThetaI*cosThetaO*cosThetaH));
    
    return fr;
  }
  
  float D(Vector3f wh) const {
    return Warp::squareToBeckmannPdf(wh,m_alpha);
  }
  // float CosTetha(Vector3f wi,Vector3f wo,Vector3f wh, Vector3f n) const{
  //   return (G(wi,wo,wh,n) * Warp::squareToBeckmannPdf(wh,m_alpha) * wo.dot(wh));
  // }

  float G(Vector3f wi,Vector3f wo, Vector3f wh, Vector3f n) const{
    return G1(wi,wh,n) * G1(wo,wh,n);
  }

  float G1 (Vector3f wv, Vector3f wh, Vector3f n) const{
    float midForm = (wv.dot(wh))/(wv.dot(n));
    float xplus = xPlus(midForm);
    //float angle =n.dot(wv);
    float angle2 = Frame::tanTheta(wv);
    float Rb = R(b(angle2));
    return xplus * Rb;
  }

  float xPlus(float c) const{
    if(c>0) {
      return 1.f;
    } else {
      return 0.f;
    }
  }

  float b(float angle) const {//angle entre la normal de la surface n et et w_v de G
    //return pow(m_alpha * tan(angle),-1);
    return pow(m_alpha * angle,-1);
  }

  float R(float b) const{
    if(b<1.6) {
      return (3.535* b + 2.181*b*b)/(1 + 2.276*b + 2.577*b*b);
    }else {
      return 1.f;
    }
  }

  /// Evaluate the sampling density of \ref sample() wrt. solid angles
  float pdf(const BSDFQueryRecord &bRec) const {
    if (Frame::cosTheta(bRec.wo) <= 0 ||Frame::cosTheta(bRec.wi) <= 0 || bRec.measure != ESolidAngle )
      return 0.0f;
    //bRec.uv
    Vector3f wh = (bRec.wi + bRec.wo).normalized();
    //cout<<"wh : "<<wh.x()<<" "<<wh.y()<<" "<<wh.z()<<endl;
    float Jh= pow(4*(wh.dot(bRec.wo)),-1);
    //cout<<"Jh : "<<Jh<<endl;
    float cosThetaO = Frame::cosTheta(bRec.wo);
    
    float res = m_ks * D(wh) * Jh + (1 - m_ks) * cosThetaO * INV_PI;
    return res ;
  }

  /// Sample the BRDF
  Color3f sample(BSDFQueryRecord &bRec, const Point2f &sample) const {
    if (Frame::cosTheta(bRec.wi) <= 0 )
      return Color3f(0.0f);
    Vector3f v = Vector3f::Zero();
    Vector3f n = Vector3f::Zero();
    Point2f p = Point2f::Zero();

    bRec.measure = ESolidAngle;
    bRec.eta = 1.0f;
    if(sample.x() < m_ks ){
      p.x() = sample.x()/m_ks;

      p.y() = sample.y();

      n = Warp::squareToBeckmann(p,m_alpha);
      bRec.wo = -bRec.wi + 2.f * bRec.wi.dot(n) * n;
    } else {
      p.x() = (sample.x()-m_ks)/(1-m_ks);
      p.y() = sample.y();   
      
      bRec.wo = Warp::squareToCosineHemisphere(p);

    }
    return eval(bRec);
  }

  bool isDiffuse() const {
    /* While microfacet BRDFs are not perfectly diffuse, they can be
       handled by sampling techniques for diffuse/non-specular materials,
       hence we return true here */
    return true;
  }

  std::string toString() const {
    return tfm::format("Microfacet[\n"
                       "  alpha = %f,\n"
                       "  intIOR = %f,\n"
                       "  extIOR = %f,\n"
                       "  kd = %s,\n"
                       "  ks = %f\n"
                       "]",
                       m_alpha, m_intIOR, m_extIOR, m_kd.toString(), m_ks);
  }

private:
  float m_alpha;
  float m_intIOR, m_extIOR;
  float m_ks;
  Color3f m_kd;
};

REGISTER_CLASS(Microfacet, "microfacet");
