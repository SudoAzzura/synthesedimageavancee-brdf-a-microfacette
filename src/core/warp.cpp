/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "warp.h"
#include "frame.h"
#include "vector.h"

Point2f Warp::squareToUniformSquare(const Point2f &sample) { 
  return sample; }

float Warp::squareToUniformSquarePdf(const Point2f &sample) {
  return ((sample.array() >= 0).all() && (sample.array() <= 1).all()) ? 1.0f
                                                                      : 0.0f;
}

Vector3f Warp::squareToUniformSphere(const Point2f &sample) {
  float z = 1.f - 2.f * sample.x();
  float r = std::sqrt(std::max(0.f, 1.f - z * z));
  float phi = 2.f * M_PI * sample.y();
  return Vector3f(r * std::cos(phi), r * std::sin(phi), z);
}

float Warp::squareToUniformSpherePdf(const Vector3f &v) { return INV_FOURPI; }

Point2f Warp::squareToUniformDisk(const Point2f &sample) {
  float r = std::sqrt(sample.x());
  float phi = 2 * M_PI * sample.y();
  //transformation coordonnées polaire en cartésienne
  float x = r * std::cos(phi);
  float y = r * std::sin(phi);
  return Point2f(x,y);
}

float Warp::squareToUniformDiskPdf(const Point2f &p) {
  //norme de  p c'est la longueur, pour savoir si c'est dans le cercle
  if(p.norm() <= 1) {
    //dans le cercle
    return INV_PI;
  }
  //hors du cercle
  return 0.f;
}

Vector3f Warp::squareToUniformHemisphere(const Point2f &sample) {
  float phi = 2.f * M_PI * sample.x();//l'angle horizontal
  float teta = std::acos(sample.y());//l'angle vertical
  
  Vector3f u = Vector3f(1,0,0);
  Vector3f v = Vector3f(0,1,0);
  Vector3f w = Vector3f(0,0,1);

  Vector3f p = std::sin(teta) * std::cos(phi) * u + 
              std::sin(teta) * std::sin(phi) * v + 
              sample.y() * w;

  return p;
}

float Warp::squareToUniformHemispherePdf(const Vector3f &v) {
  float d = pow(v.x(),2) + pow(v.y(),2) + pow(v.z(),2);

  if(sqrt(d) < 1-Epsilon || v.z() < 0||sqrt(d) > 1+Epsilon) {
    return 0.f;
    //hors de l'hémisphère
    
  }else {
  //dans l'hémisphère
  return 1/(2* M_PI);
  }
  
}

Vector3f Warp::squareToCosineHemisphere(const Point2f &sample) {
  float phi = 2.f * M_PI * sample.x();//l'angle horizontal
  float teta = std::acos(std::sqrt(1-sample.y()));//l'angle vertical
  
  Vector3f u = Vector3f(1,0,0);
  Vector3f v = Vector3f(0,1,0);
  Vector3f w = Vector3f(0,0,1);

  Vector3f p = std::sin(teta) * std::cos(phi) * u + 
              std::sin(teta) * std::sin(phi) * v + 
              std::cos(teta) * w;

  return p;
}

float Warp::squareToCosineHemispherePdf(const Vector3f &v) {
  if(v.z() > 0) {
    //dans l'hémisphère
    return 1/M_PI * v.z();
  }
  return 0.f;
}

// Point2f Warp::squareToUniformTriangle(const Point2f &sample) {
//   Point2f p = Point2f::Zero();
//   p.y() = (1-sample.y())/2;
//   //p.x() =( 1/2) - (1/2)*p.y();
//   p.x() = 1/3;
//   return p;
// }

// float Warp::squareToUniformTrianglePdf(const Point2f &p) {
//   // if(p.x()+p.y() <=1 && p.x()>=0 && p.y() >=0 )
//   //   return 2.f;
//   // else
//   //   return 0.f;
//   if(0<=p.y() && p.y()<1 && 0<=p.x() &&p.x()<=(1-p.y()) )
//     return 1/(1-p.y());
//   else
//     return 0.f;

// }

Point2f Warp::squareToUniformTriangle(const Point2f &sample) {
  // // Generate point in the unit square
  // Point2f p = Point2f(sample.x(), sample.y());

  // // Flip point with probability 1/2
  // if (p.x() + p.y() > 1) {
  //   p = Point2f(1 - p.x(), 1 - p.y());
  // }

  // // Map point from unit square to unit triangle
  // p.y() *= std::sqrt(3) / 2;
  // p.x() *= 2;

  // return p;

  // float su0 = std::sqrt(sample.x());
  // float u = 1 - su0;
  // float v = sample.y() * su0;
  // return Point2f(u, v);

   // Use a more accurate method to sample points from the triangle
  // float su0 = std::sqrt(sample.x());
  // float u = 1 - su0;
  // float v = sample.y() * su0;
  // float w = 1 - u - v;
  // Point2f p = Point2f(u / w, v / w);
  // if ((p.x() + p.y() <= 1) && (p.x() >= 0) && (p.y() >= 0)) {
  //   return p;
  // }
  // return Point2f(u / w, v / w);
}

float Warp::squareToUniformTrianglePdf(const Point2f &p) {
  // Check if point is inside triangle
  if ((p.x() + p.y() <= 1) && (p.x() >= 0) && (p.y() >= 0)) {
    return 2.0f;
  }
  return 0.0f;

  //   if (p.x() >= 0 && p.x() <= 1 && p.y() >= 0 && p.y() <= std::sqrt(3) / 2 && p.y() <= p.x()) {
  //   // The point is within the triangle, so return the probability density
  //   return 2 / std::sqrt(3);
  // } else {
  //   // The point is outside the triangle, so return 0
  //   return 0;
  // }
}

Vector3f Warp::squareToBeckmann(const Point2f &sample, float alpha) {
  float phi = 2.f * M_PI *sample.x();
  float alpha2 = pow(alpha,2);

  float theta = atan( sqrt( -alpha2 * log(1 - sample.y()) ) );
  
  float x = sin(theta) * cos(phi);
  float y = sin(theta) * sin(phi);
  float z = cos(theta);

  return Vector3f(x,y,z);
}

float Warp::squareToBeckmannPdf(const Vector3f &m, float alpha) {
  float cosTheta = Frame::cosTheta(m);
  if( cosTheta <=0 ) {
    return 0.f;
  }
  float tanteta = Frame::tanTheta(m);
  float teta = acos(cosTheta);
  //float sinteta = sin(teta);
  float sinteta = Frame::sinTheta(m);

  //float tan2 = pow(tanteta,2);
  float tan2 = (1-cos(2*teta))/(1+cos(2*teta));
  //float cos3 = pow(cosTheta,3); 
  float cos3 = (cos(3*teta)+3*cos(teta))/4;
  float alpha2 = pow(alpha,2);

  float p2 = 2* exp((-tan2)/alpha2);
  float p3 = alpha2 * cos3;

  return INV_TWOPI * (p2/p3);
}
