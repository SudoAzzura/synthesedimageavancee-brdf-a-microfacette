#include "lights/areaLight.h"
#include "core/bsdf.h"
#include "core/texture.h"
#include "core/warp.h"
#include <math.h>

AreaLight::AreaLight(const PropertyList &propList) {
  m_intensity = propList.getColor("radiance", Color3f(1.f));
  m_twoSided = propList.getBoolean("twoSided", false);
  m_texture = new Texture(m_intensity, propList);
  m_flags = (int)LightFlags::Area;
}

void AreaLight::addChild(Object *obj) {
  switch (obj->getClassType()) {
  case EShape:
    if (m_shape)
      throw RTException(
          "AreaLight: tried to register multiple shape instances!");
    m_shape = static_cast<Shape *>(obj);
    break;
  default:
    throw RTException("AreaLight::addChild(<%s>) is not supported!",
                      classTypeName(obj->getClassType()));
  }
}

Color3f AreaLight::intensity(const Hit &hit, const Vector3f &w) const {
  if(!hit.foundIntersection())
     return Color3f(0.f);
  Color3f radiance  = Color3f(1.f,1.f,1.f);

  Color3f intensity = m_intensity/w.squaredNorm();
  Normal3f normal = hit.localFrame.n;

  
  //si les deux faces n'émettent pas de la lumière
  double pi = 3.14159;
  //double a = normal.dot(w) /*/ (normal.normalized()).dot(w.normalized())*/;
  double a = w.dot(normal);
  float anglerad = acos(a);
  float angledegree = anglerad * (180 / pi);
  //cout<<angledegree<<endl;
  //if(!m_twoSided) {
    //Si face avant mais ça ne marche pas
    if(anglerad<=2*pi) {
      
      //intensity = Color3f(1.0f,1.0f,1.0f);
      radiance = std::max(0.f,w.normalized().dot(normal.normalized())) * intensity;
      if(m_texture) {
        radiance = Color3f(m_texture->lookUp(hit.uv));
      }      
    } else {
      radiance  = Color3f(0.f,0.f,0.f);
    }

     return radiance;
}

Color3f AreaLight::sample(const Point3f &x, const Point2f &u, float &pdf,
                          Vector3f &wi, float &dist) const {
  Point3f p;
  Normal3f n;
  m_shape->sample(u,p,n,pdf);
  wi = (p-x).normalized();
  
  float radiance = ( m_shape->area() * cos(n.dot(p-x)))/(dist*dist);

  return Color3f(radiance,radiance,radiance);
}

std::string AreaLight::toString() const {
  return tfm::format("AreaLight[\n"
                     "  intensity = %s,\n"
                     "  two-sided = %f,\n"
                     "  shape = %f,\n"
                     "]",
                     m_intensity.toString(), m_twoSided ? "true" : "false",
                     indent(m_shape->toString()));
}

REGISTER_CLASS(AreaLight, "areaLight")
