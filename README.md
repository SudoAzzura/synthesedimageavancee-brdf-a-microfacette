# TD5-6 - SIA - BRDF à Microfacette & Path Tracing- Chloé Neuville
## 1. Évaluation et échantillonnage de la distribution de Beckmann
### 1.1. Échauffement : échantillonnage du disque unitaire

![alt text](./dataMd/wrapTestBeck01.png)
![alt text](./dataMd/wrapTestBeck01Pdf.png)

![alt text](./dataMd/wrapTestBeck05.png)
![alt text](./dataMd/wrapTestBeck05Pdf.png)

![alt text](./dataMd/wrapTestBeck1.png)
![alt text](./dataMd/wrapTestBeck1Pdf.png)


## 2. Évaluation de la BRDF à microfacettes
Après avoir codé la formule donnée pour cette partie je me suis retrouvée bloquée longtemps pour plusieur raison et la toute prmière est simplement une confusion en lisant la formule. Avec la fatigue et le stress les erreurs bêtes s'accumulent :

![alt text](./dataMd/confusionFormule.png)

Je croyais face à cette formule que la fonction X+ était multipliée avec ce qui la suis entre parenthèse, je n'avais pas compris que le fameux c que je m'étais retrouvée à chercher dans le cours, n'était autre que ça. 
Je suis donc restée pas mal de temps avec ce genre de résultat pas du tout engageant pour ajax ayant un alpha de 0.03 :

![alt text](./dataMd/ajax_microfacet4.png)


Puis j'ai aussi fais des modifications, ou plutôt des expérimenttaions, pour améliorer mon résultat qui correspondait faiblement.
J'avais enlevé le "*4" dans la division ce qui me donnait :


![alt text](./dataMd/ajax_microfacet6.png)

Ensuite, quand j'ai réussi à me débarasser de cette erreur, j'ai eu de nouveau problème, l'image était trop clair, "surrexposée" et c'était belle et bien dû au "*4" absent.

Voici les résultats pour cette partie après avoir réglé tous les problèmes :

alpha = 0.03
![alt text](dataMd/ajax_microfacet003.png)

alpha = 0.1
![alt text](dataMd/ajax_microfacet01.png)

alpha = 0.3
![alt text](dataMd/ajax_microfacet03.png)

alpha = 0.6
![alt text](dataMd/ajax_microfacet06.png)


## 3. Échantillonnage uniforme de la BRDF
Manquait un test if donc pour par exemple alpha = 0.6, ça donnait :

![alt text](dataMd/ajax_microfacet_envmap_grain_de_beaute_blanc_nez.png)
Dans ce if j'avais gardé "if (!shadowHit.foundIntersection() || shadowHit.t > dist) {"
la condition "shadowHit.t > dist" alors qu'il n'ait plus question de dist et que celui-ci était quand même déclaré cela produisait cette erreur de grain de beauté blanc sur le nez d'ajax.


Puis maintenant ça donne ça :
alpha = 0.03
![alt text](dataMd/ajax_microfacet_envmap003.png)

alpha = 0.1
![alt text](dataMd/ajax_microfacet_envmap01.png)

alpha = 0.3
![alt text](dataMd/ajax_microfacet_envmap03.png)

alpha = 0.6
![alt text](dataMd/ajax_microfacet_envmap06.png)

## 4. Échantillonnage préférentiel de la BRDF

Au début, j'avais ce résultat pour : alpha = 0.2, k_d = 0.1, Teta = -46.2

![alt text](dataMd/EchBrdf02_-46_2pasBien1.png)
![alt text](dataMd/EchBrdf02_-46_2pasBien2.png)

Mon code passe tous les tests de : ../scenes/tests/chi2test-microfacet.scn
Mais aucun de : ../scenes/tests/chi2test-microfacet.scn

J'ai alors parcouru mon code à la recherche d'erreur, qui pouvait amener à ces résultats médiocres.
J'en ai trouvé une, un problème d'angle dans la fonction b utilisé dans eval:
Pour calculer la tangeante j'utilisais tan au lieu d'utiliser la fonction pour cet effet dans Frame: je fais donc Frame::tanTheta(wv) au lieu de tan(n.dot(wv))

Ce qui donne :

Pour alpha = 0.1 , k_d = 0.1 , Tetha = -36.5 :


![alt text](dataMd/EchBrdf01_-36_5.png)
![alt text](dataMd/EchBrdf01_-36_5pdf.png)


Pour alpha = 0.2 , k_d = 0.1 , Tetha = -36.5 :


![alt text](dataMd/EchBrdf02_-36_5.png)
![alt text](dataMd/EchBrdf02_-36_5pdf.png)


Pour alpha = 0.2 , k_d = 0.1 , Tetha = -46.2 :


![alt text](dataMd/EchBrdf02_-46_2.png)
![alt text](dataMd/EchBrdf02_-46_2pdf.png)


Mais mon code ne passe toujours pas le test : ../scenes/tests/chi2test-microfacet.scn


J'ai encore remonté le temps, et je suis arrivée dans la fonction Warp::squareToBeckmannPdf


j'ai passé : float tan2 = pow(tanteta,2);


à : float tan2 = (1-cos(2*teta))/(1+cos(2*teta));


et j'ai passé : float cos3 = pow(cosTheta,3); 


à : float cos3 = (cos(3*teta)+3*cos(teta))/4;

Et ça donne :


![alt text](dataMd/EchBrdf02_-46_2apresmodif1.png)
![alt text](dataMd/EchBrdf02_-46_2apresmodif2.png)

J'ai cru sur le moment que ça avait amélioré quelque chose mais en comparant avec les captures, ça ne semble pas avoir changé, au moins ça n'a pas agravé la situation mais le test ne passe toujours pas.
Puis j'ai regardé ce que donnait mes modifcations à ajax_microfacet_envmap.scn, le résultat était clairement meilleur et je pense que c'est grâce à la première modification, Frame::tanTheta(wv) au lieu de tan(n.dot(wv)) dans eval:

alpha = 0.03
![alt text](dataMd/ajax_microfacet_envmaplast003.png)

alpha = 0.1
![alt text](dataMd/ajax_microfacet_envmaplast01.png)

alpha = 0.3
![alt text](dataMd/ajax_microfacet_envmaplast03.png)

alpha = 0.6
![alt text](dataMd/ajax_microfacet_envmaplast06.png)

Je passe donc à la suite en utilisant sample et pdf lorsquele tag IS est à vrai.

## 4.2 Échantillonnage préférentiel de la BRDF integrateur
alpha = 0.03 Is = true N=32
![alt text](./dataMd/ajax_microfacet_envmap003true.png)


alpha = 0.03 Is = false N=32
![alt text](dataMd/ajax_microfacet_envmaplast003.png)



alpha = 0.1 Is = true N=32
![alt text](./dataMd/ajax_microfacet_envmap01true.png)


alpha = 0.1 Is = false N=32
![alt text](dataMd/ajax_microfacet_envmaplast01.png)



alpha = 0.3 Is = true N=32
![alt text](./dataMd/ajax_microfacet_envmap03true.png)

alpha = 0.3 Is = false N=32
![alt text](dataMd/ajax_microfacet_envmaplast03.png)



alpha = 0.6 Is = true N=32
![alt text](./dataMd/ajax_microfacet_envmap06true.png)

alpha = 0.6 Is = false N=32
![alt text](dataMd/ajax_microfacet_envmaplast06.png)

Lorsque alpha vaut 0.03 et 0.1 et que IS est à true ajax a des reflets métaliques par rapport à quand IS est à false, mais pas assez à mon goût, cela ne ressemble pas assez au image exemple. Mais j'ai remarqué que lorsque l'on augmente l'échantillonage l'image de ajax match plus avec les exemples.

## TP6-1. tracé de chemin par échantillonnage de la BSDF

Je suis longtemps restée blockée sur cette erreur lors de l'ajout de path_mats.cpp:

/home/chloe/Documents/FAC/Master_info/M2/Monde3d_SIA_SyntheseDImageAvancéé/TP5/sia-brdf-a-microfacettes/src/core/object.h:133:28: error: invalid new-expression of abstract class type ‘PathMats’
  133 |         return new cls(list); \
      |                            ^
/home/chloe/Documents/FAC/Master_info/M2/Monde3d_SIA_SyntheseDImageAvancéé/TP5/sia-brdf-a-microfacettes/src/integrators/pathMats.cpp:27:1: note: in expansion of macro ‘REGISTER_CLASS’
   27 | REGISTER_CLASS(PathMats, "path_mats")
      | ^~~~~~

Qui était dû au fait qu'il me manquait une mise à jour.

Puis j'ai obtenu ça, en ne récupérant que la couleur et sans faire de chemin:

![alt text](dataMd2/cbox_mats.png)

Puis j'ai ajouté les chemins mais il manquait les lumières :

![alt text](dataMd2/cbox_mats2.png)


![alt text](dataMd2/cbox_mats4.png)
![alt text](dataMd2/veach_mats.png)

Puis s'en est suivie une série de test pour comprendre d'où venait mon manque de couleur:

![alt text](dataMd2/cbox_mats5.png)
![alt text](dataMd2/cbox_mats6.png)
![alt text](dataMd2/cbox_mats7.png)

Puis j'ai compris, il fallait sortir le test de non-détection d'intersection de la boucle des chemin et le faire avant.

![alt text](dataMd2/cbox_mats8.png)
![alt text](dataMd2/veach_mats2.png)

Mais le résultat n'est pas non plus satisfaisant
